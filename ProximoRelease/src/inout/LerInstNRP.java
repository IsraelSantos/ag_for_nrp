package inout;

public class LerInstNRP {
	
	private int qClientes;
	private int qRequisitos;
	private double [] impor;
	private int [][] nota;
	private int [] custos;
	private FileIn r=null;
	
	public LerInstNRP(String nameFile) {
		r=new FileIn(nameFile);
		this.qClientes=r.nextInt();
		this.qRequisitos=r.nextInt();
		impor=new double[this.qClientes];
		nota=new int[this.qRequisitos][this.qClientes];
		this.custos= new int[this.qRequisitos];
		//Ler importāncia de cada Cliente
		for (int i=0;i<this.qClientes;i++){
			impor[i]=r.nextDoublePonto();
		}
		//Ler nota que cada cliente deu para cada requisito
		for (int i=0;i<this.qClientes;i++)
			for (int j=0; j<this.qRequisitos; j++){
				nota[j][i]=r.nextInt();
			}
		//Ler vetor de custos dos requisitos
		for (int i=0; i< this.qRequisitos; i++){
			custos[i]=r.nextInt();
		}
	}
	
	public int getqClientes() {
		return qClientes;
	}
	public int getqRequisitos() {
		return qRequisitos;
	}
	public double[] getImpor() {
		return impor;
	}
	public int[][] getNota() {
		return nota;
	}
	public int[] getCustos() {
		return custos;
	}
	
	public void imprimir(){
		System.out.println(this.qClientes+" "+this.qRequisitos+"\n");
		//Importāncia de cada Cliente
		for (int i=0;i<this.qClientes;i++){
			System.out.print(impor[i]+" ");
		}
		System.out.print("\n\n");
		//Nota que cada cliente deu para cada requisito
		for (int i=0;i<this.qClientes;i++){
			for (int j=0; j<this.qRequisitos; j++){
				System.out.print(nota[j][i]+" ");
			}
			System.out.print("\n");
		}
		System.out.print("\n");
		//Vetor de custos dos requisitos
		for (int i=0; i< this.qRequisitos; i++){
			System.out.print(custos[i]+" ");
		}
	}
	
	public static void main(String[] args) {
		LerInstNRP re=new LerInstNRP("arquivos//I_S_25_5.txt");
		re.imprimir();
	}
	
}
