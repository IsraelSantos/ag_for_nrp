package ctr;

import java.util.List;
import util.Random;

public class CruzamentoVarios implements Cruzamento {

	@Override
	public int[] cruzamento(List<IndividuoNRP> listInd) {
		int []sq2 = new int [listInd.get(0).getTamanho()];
		Random r=new Random();
		for (int i = 0, j = 0; i<sq2.length; i++){
			sq2[i] = listInd.get(j).getSq(i);
			if (j<listInd.size()-1 && Math.abs(r.nextInt())%2 == 0)
				j++;
		}
		return sq2;
	}

}
