package ctr;

import java.util.List;

public interface Cruzamento {

	public int[] cruzamento(List<IndividuoNRP> listInd);
	
}
