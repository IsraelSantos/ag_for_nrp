package ctr;

import inout.LerInstNRP;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import util.Random;

public class AlgoritmoGeneticoNRP {
	List <IndividuoNRP> lis;
	double taxaMorte;
	IndividuoNRP melhorIndividuo;
	IndividuoNRP melInicial;
	IndividuoNRP piorIndiv;
	double resultMedio=0;
	double probabilidadePermut;
	int numG;
	LerInstNRP inst;
	double recursosDisponiveis;
	Cruzamento cruza;
	
	public AlgoritmoGeneticoNRP(int tamanho, double probabilidade, double taxaMorte, int numGera, LerInstNRP ins, double recursos, Cruzamento cruza){
		this.probabilidadePermut = probabilidade;
		this.taxaMorte = taxaMorte;
		this.numG=numGera;
		this.inst=ins;
		this.recursosDisponiveis=recursos;
		this.cruza = cruza;
		lis = new ArrayList<IndividuoNRP>();
		for (int i=0;i<tamanho;i++){
			lis.add(new IndividuoNRP(inst, recursosDisponiveis, this.cruza));
		}
		Collections.sort(lis);
		melInicial=melhorIndividuo=lis.get(0);
		piorIndiv=lis.get(lis.size()-1);
	}
	
	public void AG(){
		Random a=new Random();
		int testeParada=0;
		while (testeParada<100){//Gera��es
			for(int j=lis.size()-1;j>lis.size()-(taxaMorte*lis.size());j--){
				List<IndividuoNRP> listInd = new ArrayList<IndividuoNRP>();
				int tamElite = (int)(lis.size()-(taxaMorte*lis.size())); //Tamanho da elite
				if (! (this.cruza instanceof CruzamentoVarios)){
					//Escolhe dois individuos que ir�o sobreviver para cruzar...
					int t1=Math.abs(a.nextInt(tamElite));
					int t2=Math.abs(a.nextInt(tamElite));
					listInd.add(lis.get(t1));
					listInd.add(lis.get(t2));
				}else{
					Random r = new Random();
					int qRelacionamentos = ((Math.abs(r.nextInt())% (tamElite-2))+ 2)/2;
					for (int i=0; i<=qRelacionamentos; i++){
						int t1=Math.abs(a.nextInt(tamElite));
						listInd.add(lis.get(t1));
					}
				}
				//A suruba...
					lis.set(j, new IndividuoNRP(inst, listInd, probabilidadePermut, recursosDisponiveis, this.cruza));//Substitue os individuos mais fracos por novos.
			}
			Collections.sort(lis);
			resultMedio+=lis.get(0).getFitness();
			if(lis.get(0).compareTo(melhorIndividuo)<0){
				melhorIndividuo=lis.get(0);
				testeParada=0;
			}else{
				testeParada++;
			}
			if(lis.get(lis.size()-1).compareTo(piorIndiv)>0){
				piorIndiv=lis.get(lis.size()-1);
			}
		}
		
		resultMedio=resultMedio/numG;
	}
	

	public double getResultMedio() {
		return resultMedio;
	}

	public IndividuoNRP getPiorIndiv() {
		return piorIndiv;
	}

	public IndividuoNRP getMelhorIndividuo() {
		return melhorIndividuo;
	}

	public IndividuoNRP getMelInicial() {
		return melInicial;
	}
	
	
}
