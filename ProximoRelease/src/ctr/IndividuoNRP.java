package ctr;

import inout.LerInstNRP;

import java.util.List;
import java.util.Random;

public class IndividuoNRP implements Comparable{
	
	int []sq;
	double fitness;
	LerInstNRP instNRP;
	double recursosDisponiveis;
	Cruzamento cruza;
	

	public IndividuoNRP(LerInstNRP ins, double recursos, Cruzamento cruza){
		sq=new int[ins.getqRequisitos()];
		instNRP=ins;
		this.cruza=cruza;
		this.recursosDisponiveis=recursos;
		geraIndividuo();
		fitness=fitness(instNRP);
	}

	public IndividuoNRP(LerInstNRP ins, List<IndividuoNRP> listInd, double probabiliade, double recursos, Cruzamento cruza){
		instNRP=ins;
		this.recursosDisponiveis=recursos;
		this.cruza=cruza;
		sq=cruza.cruzamento(listInd);
		permut(probabiliade);
		restauraIndividuo();
		fitness=fitness(instNRP);
	}

	public void permut(double probabilidade){
		Random a=new Random();
		for(int i = 0; i < sq.length; i++){
				double t = Math.random();
				if(a.nextInt()%1047293==0 && t<probabilidade){
					//System.out.println("Estou aqui!");
					if (sq[i]==0)
						sq[i]=1;
					else sq[i]=0;
				}
		}
	}
	
	public double getFitness() {
		return fitness;
	}

	public int getTamanho(){
		return sq.length;
	}

	public int getSq(int i) {
		return sq[i];
	}

	public void setSq(int[] sq) {
		this.sq = sq;
	}

	public double fitness(LerInstNRP inst) {
		double res=0;
		for (int i=0;i<sq.length-1;i++){
			if (sq[i]==1){
				double score=0;
				for(int j=0; j < inst.getqClientes(); j++)
					score += inst.getImpor()[j]*inst.getNota()[i][j];
				res+=score*sq[i];
			}
		}
		return res;
	}

	public void geraIndividuo(){
		Random a=new Random();
		//Gera um individuo aleat�rio
		for (int i = 0; i < sq.length; i++) {
			if (Math.abs(a.nextInt())%2==0){
				sq[i]=1;
			}else 
				sq[i]=0;
		}
		//Verifica se o individuo � v�lido (obedece � restri��o de custo), sen�o, tenta consert�-lo.
		restauraIndividuo();
	}
	
	//M�todo para restaura��o dos indiv�duos que n�o seguem a restri��o de custo.
	public void restauraIndividuo(){
		Random a=new Random();
		while (!isValido()){
			int posicaoAleatoria = Math.abs(a.nextInt(sq.length));
			sq[posicaoAleatoria]=0;
		}
	}
	
	public long custo(){
		long somCustos=0;
		for (int i=0; i<instNRP.getqRequisitos(); i++){
			somCustos+=this.sq[i]*instNRP.getCustos()[i];
		}
		return somCustos;
	}
	
	public boolean isValido(){
		return (custo() <= this.recursosDisponiveis);
	}
	
	public String toString(){
		String res="";
		for(int i=0;i<instNRP.getqRequisitos();i++)
			res+=sq[i]+"\t|";
		res+="\n";
		return res;
	}
	
	public int compareTo(Object arg0) {
		if (arg0 instanceof IndividuoNRP){
			IndividuoNRP tmp=(IndividuoNRP) arg0;
			if (this.getFitness()<tmp.getFitness())
			  return 1;
			else 
				if (this.getFitness()>tmp.getFitness())
					return -1;
				else
					return 0;
		}
		return 0;
	}
}
