package ctr;

import java.util.List;
import util.Random;

public class CruzamentoNPonto implements Cruzamento {

	@Override
	public int[] cruzamento(List<IndividuoNRP> listInd) {
		int []sq2 = new int [listInd.get(0).getTamanho()];
		int i;
		Random r=new Random();
		boolean primeiro=true;
		for (i = 0; i<sq2.length; i++){
			if (primeiro)
				sq2[i] = listInd.get(0).getSq(i);
			else 
				sq2[i] = listInd.get(1).getSq(i);
			if (Math.abs(r.nextInt())%2 == 0)
				primeiro = !primeiro; 
		}
		return sq2;
	}

}
