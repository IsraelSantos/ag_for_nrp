package util;

public class Random {
	private int [] vet = new int[3];
	int t=-1;
	private void fill(){
		double man = Math.abs(Math.random());
		for (int i=0; i<3; i++){
			man *= 10000000;
			vet[i] = (int) man;
			man -= vet[i];
			t++;
		}
	}
	
	public int nextInt(){
		if (empty())
			fill();
		return vet[t--];
	}
	
	public int nextInt(int q){
		if (empty())
			fill();
		return vet[t--]%q;
	}
	
	private boolean empty(){
		return t==-1;
	}
}
