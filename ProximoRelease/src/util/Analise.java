package util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;

public class Analise {
	private static final BigDecimal SQRT_DIG = new BigDecimal(150);
	public List<BigDecimal> values;
	
	public Analise(){
		values= new ArrayList<BigDecimal>();
	}
	
	public void add(double value){
		values.add(new BigDecimal(value));
	}
	
	public void clean(){
		values.clear();
	}
	
	public BigDecimal esperancaQuad(){//E(X^2) -> Dado que todos os elementos tem a mesma probabilidade de ocorrer este valor � a m�dia.
		BigDecimal res= new BigDecimal(0);
		for (int i=0; i<values.size(); i++){
			res=res.add(values.get(i).multiply(values.get(i)));
		}
		return res.divide(new BigDecimal(values.size()),2*SQRT_DIG.intValue(),RoundingMode.HALF_DOWN);
	}
	
	public BigDecimal esperanca(){//E(X) Dado que todos os elementos tem a mesma probabilidade de ocorrer este valor � a m�dia.
		BigDecimal res= new BigDecimal(0);
		for (int i=0; i<values.size(); i++){
			res=res.add(values.get(i));
		}
		return res.divide(new BigDecimal(values.size()),2*SQRT_DIG.intValue(),RoundingMode.HALF_DOWN);
	}
	
	public BigDecimal desvioPadrao(){
		BigDecimal espe=esperanca();
		return BigSqrt.bigSqrt(esperancaQuad().subtract(espe.multiply(espe))); // sqrt(E(X^2) - E(X)^2)
	}
}
