package teste;

import inout.FileOut;
import inout.LerInstNRP;

import java.text.DecimalFormat;
import java.util.Scanner;

import util.Analise;
import ctr.AlgoritmoGeneticoNRP;
import ctr.Cruzamento;
import ctr.CruzamentoNPonto;
import ctr.CruzamentoUmPonto;
import ctr.CruzamentoVarios;
import ctr.IndividuoNRP;

public class ClassTesteNRP {
	
		public static void main(String[] args) {

			int tamPop=1000;
			double probabilidadePermult=0.01;
			double taxaMorte=0.5;
			int numGera=1000;
			int numeroDeVezes=30;//Quantidade de vezes que ser� executada dada inst�ncia...
			FileOut f=new FileOut("saidaCruzamentoVarios.txt");
			Cruzamento cruza = new CruzamentoVarios();
			
			if (args.length!=0){
				
				double recursos;
				Scanner sc=new Scanner(System.in);
				System.out.println("Diga quanto voc� tem de recursos:");
				recursos = sc.nextDouble();
				sc.close();
				
				LerInstNRP l=new LerInstNRP(args[0]);
				double res=0;
				double meini=0;
				long tempo=0;
				double custoFinal=0;
				for (int j = 0; j < numeroDeVezes; j++) {//"Execut�-lo pelo menos tr�s vezes para cada inst�ncia."
					long ini=System.currentTimeMillis();
					AlgoritmoGeneticoNRP k=new AlgoritmoGeneticoNRP(tamPop,
							probabilidadePermult,
							taxaMorte,
							numGera,
							l,
							recursos,
							cruza);
					k.AG();
					long fim=System.currentTimeMillis();
					res+=k.getMelhorIndividuo().getFitness();
					meini+=k.getMelInicial().getFitness();
					tempo+=fim-ini;
					custoFinal+=k.getMelhorIndividuo().custo();
				}
				DecimalFormat fmt = new DecimalFormat("0.00");
				String saida=1+"\t|"+args[0]+"\t|"+fmt.format(meini/numeroDeVezes).toString()+"\t|"+(tempo/numeroDeVezes)+"\t|"+fmt.format(custoFinal/numeroDeVezes)+"\t|"+fmt.format(res/numeroDeVezes);

				System.out.println(saida);
				f.println(saida);
			}else{


				String vet[]=new String[17];
				vet[0]="dataset-1";
				vet[1]="dataset-2";
				vet[2]="I_S_100_25";
				vet[3]="I_S_100_5";
				vet[4]="I_S_100_50";
				vet[5]="I_S_250_25";
				vet[6]="I_S_250_5";
				vet[7]="I_S_250_50";
				vet[8]="I_S_25_25";
				vet[9]="I_S_25_5";
				vet[10]="I_S_25_50";
				vet[11]="I_S_500_25";
				vet[12]="I_S_500_5";
				vet[13]="I_S_500_50";
				vet[14]="I_S_50_25";
				vet[15]="I_S_50_5";
				vet[16]="I_S_50_50";

				System.out.println("Executando...");
				for (int i=0;i<17;i++){
					LerInstNRP l=new LerInstNRP("arquivos//"+vet[i]+".txt");
					Analise res = new Analise();
					Analise meini = new Analise();
					Analise tempo = new Analise();
					Analise pior = new Analise();
					Analise resultMedio = new Analise();
					double recursos=0;
					Analise custoFinal = new Analise();
					IndividuoNRP melhor = null;
					
					//C�lculo dos recursos de teste
					for (int j=0;j<l.getqRequisitos();j++)
						recursos+=l.getCustos()[j];
					
					recursos=recursos*(1.0f/2.0f);//Metade do n�mero m�ximo do custo.
					//System.out.println("Recurso dispon�vel: "+recursos);
					for (int j = 0; j < numeroDeVezes; j++) {//"Execut�-lo pelo menos tr�s vezes para cada inst�ncia."
						long ini=System.currentTimeMillis();
						AlgoritmoGeneticoNRP k=new AlgoritmoGeneticoNRP(tamPop,
								probabilidadePermult,
								taxaMorte,
								numGera,
								l,
								recursos,
								cruza);
						k.AG();
						long fim=System.currentTimeMillis();
						res.add(k.getMelhorIndividuo().getFitness());//
						if (j==0)
							melhor = k.getMelhorIndividuo();
						else
							if (melhor.getFitness() < k.getMelhorIndividuo().getFitness())
								melhor = k.getMelhorIndividuo();
						meini.add(k.getMelInicial().getFitness());
						tempo.add(fim-ini);
						pior.add(k.getPiorIndiv().getFitness());//
						resultMedio.add(k.getResultMedio());
						custoFinal.add(k.getMelhorIndividuo().custo());
					}
					DecimalFormat fmt = new DecimalFormat("0.000");
					String saida=(i+1)+"\t"+vet[i]+"\t"+recursos+"\t"+fmt.format(pior.esperanca())+"+-"+fmt.format(pior.desvioPadrao())+"\t"+fmt.format(res.esperanca())+"+-"+fmt.format(res.desvioPadrao())+"\t"+fmt.format(custoFinal.esperanca())+"+-"+ fmt.format(custoFinal.desvioPadrao()) +"\t"+melhor.custo()+"\t"+tempo.esperanca().longValue()+"+-"+tempo.desvioPadrao().longValue();
					System.out.println(saida);
					//System.out.println("Custo do melhor indiv�duo: "+melhor.custo()+"\n"+melhor);
					f.println(saida);
					//f.println("Custo do melhor indiv�duo: "+melhor.custo()+"\n"+melhor);
				}

				f.close();
			}


		}


}
